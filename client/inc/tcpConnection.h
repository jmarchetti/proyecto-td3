/*
 * ------------- TCP Connection Library --------------
 * 
 * Library for tcp connections.
 */

#ifndef __TCPCONNECTION_H__
#define __TCPCONNECTION_H__
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>

#define BACKLOG 5

typedef struct 
{
    int tcpSockFd;
    struct sockaddr_in tcpMyAddr;     /* contendra la direccion IP y el numero de puerto local */
    struct sockaddr_in tcpTheirAddr;  /* Contendra la direccion IP y numero de puerto del cliente */
    int sinSize;

} tcpStruct;


/* Function tcpInit for server side. 
 * Parameters:
 *  - tcpStruct_ptr: Tipo tcpStruct. Puntero a la estructura a inicializar.
 *  - port: Tipo entero. Puerto en el cual se inicializará el socket TCP.
 * Returns:
 *  - Si inicializó correctamente retorna 0, sino retorna el código de error negativo. 
 */

int tcpInitServer(tcpStruct *tcpStruct_ptr, int port);

/* Function tcpInit for client  side. 
 * Parameters:
 *  - tcpStruct_ptr: Tipo tcpStruct. Puntero a la estructura a inicializar.
 *  - server: Tipo char. Url o IP a la que se conectará.
 *  - port: Tipo entero. Puerto en el cual se inicializará el socket TCP.
 * Returns:
 *  - Si inicializó correctamente retorna 0, sino retorna el código de error negativo. 
 */

int tcpInitClient(tcpStruct *tcpStruct_ptr, char *server, int port);

/* Function tcpAccept (bloqueante)
 * Parameters:
 *  - tcpStruct_ptr: Tipo puntero tcpStruct. Puntero a la estructura ya inicializada.
 *  - tcpStructNew_ptr: Tipo puntero tcpStruct. Puntero a la nueva estructura que contendrá los datos de la conexión iniciada.
 *  - timeout: Tipo entero. Timeout opcional. Si es 0, será completamente bloqueante.
 * Returns:
 *  - Si inicializó correctamente retorna 1, si salió por timeout retorna 0, sino retorna el código de error negativo.
 */

int tcpAccept(tcpStruct *tcpStruct_ptr, tcpStruct *tcpStructNew_ptr, int timeout);

/* Function tcpSend (bloqueante)
 * Parameters:
 *  - tcpStruct_ptr: Tipo puntero tcpStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Tipo puntero void. Puntero a los datos a enviar.
 *  - size: Tipo entero. Tamaño en bytes de los datos a enviar.
 * Returns:
 *  - Si inicializó correctamente retorna la cantidad de bytes enviados, sino retorna el código de error negativo.
 */

int tcpSend(tcpStruct *tcpStruct_ptr, const void *buffer, int size);

/* Function tcpReceive (bloqueante)
 * Parameters:
 *  - tcpStruct_ptr: Tipo puntero tcpStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Tipo puntero void. Puntero donde se guardarán los datos.
 *  - size: Tipo entero. Tamaño en bytes máximo de lectura.
 *  - timeout: Tipo entero. Timeout opcional. Si es 0, será completamente bloqueante.
 * Returns:
 *  - Si inicializó correctamente retorna la cantidad de bytes recibidos, sino retorna el código de error negativo.
 */

int tcpReceive(tcpStruct *tcpStruct_ptr, void *buffer, int size, int timeout);

/* Function tcpClose
 * Parameters:
 *  - tcpStruct_ptr: Tipo puntero tcpStruct. Puntero a la estructura ya inicializada.
 */

void tcpClose(tcpStruct *tcpStruct_ptr);

#endif