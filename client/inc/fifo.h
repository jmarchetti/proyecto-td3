#ifndef __FIFO_H__
#define __FIFO_H__
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

typedef struct 
{
    char name[20];
} fifoStruct;

/* Function fifoCreate. 
 * Parameters:
 *  - fifoStruct_ptr: Tipo puntero a fifoStruct. Puntero a la estructura a inicializar.
 *  - name: Array Char. Nombre que se le dará a la fifo (máximo 19 caracteres).
 * Returns:
 *  - Si inicializó correctamente retorna 0, sino retorna el código de error negativo. 
 */

int fifoCreate(fifoStruct *fifoStruct_ptr, const char *name);

/* Function fifoRead. 
 * Parameters:
 *  - fifoStruct_ptr: Tipo puntero a fifoStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Puntero void. Puntero a donde guardar lo recibido.
 *  - size: Entero. Tamaño en bytes máximo de los datos a recibir.
 * Returns:
 *  - Si recibió correctamente retorna la cantidad de bytes recibidos, sino retorna el código de error negativo. 
 */
int fifoRead(fifoStruct *fifoStruct_ptr, void *buffer, int size);
/* Function fifoWrite. 
 * Parameters:
 *  - fifoStruct_ptr: Tipo puntero a fifoStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Puntero void. Puntero a los datos a escribir.
 *  - size: Entero. Tamaño en bytes de los datos a escribir.
 * Returns:
 *  - Si escribió correctamente retorna la cantidad de bytes escritos, sino retorna el código de error negativo. 
 */
int fifoWrite(fifoStruct *fifoStruct_ptr, void *buffer, int size);

#endif