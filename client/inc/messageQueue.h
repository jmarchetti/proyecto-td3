#ifndef __MESSAGEQUEUE_H__
#define __MESSAGEQUEUE_H__
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <string.h>

#define MESSAGE_LENGTH 200

typedef struct 
{
    long mtype;
    char message[MESSAGE_LENGTH];
} msgBuf;


typedef struct 
{
    key_t mqKey;
    msgBuf mQ;
    int mqId;
} mqStruct;

/* Function mqCreate. 
 * Parameters:
 *  - mqStruct_ptr: Tipo puntero a mqStruct. Puntero a la estructura a inicializar.
 *  - key: Puntero a char. Nombre para generar la key.
 *  - keyChar: Tipo char. Caracter para generar la key.
 * Returns:
 *  - Si inicializó correctamente retorna 0, sino retorna el código de error negativo. 
 */

int mqInit(mqStruct *mqStruct_ptr, const char *key, char keyChar);

/* Function mqSend. 
 * Parameters:
 *  - mqStruct_ptr: Tipo puntero a mqStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Puntero void. Puntero a los datos a enviar.
 *  - size: Entero. Tamaño en bytes de los datos a enviar.
 *  - mtype: Long. Tipo de mensaje a enviar.
 * Returns:
 *  - Si se envió correctamente retorna 0, sino retorna error negativo. 
 */
int mqSend(mqStruct *mqStruct_ptr, void *buffer, int size, long mtype);

/* Function mqRecv. 
 * Parameters:
 *  - mqStruct_ptr: Tipo puntero a mqStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Puntero void. Puntero donde guardar los datos recibidos.
 *  - size: Entero. Tamaño en bytes de los datos a recibir.
 *  - mtype: Long. Tipo de mensaje a recibir
 * Returns:
 *  - Si recibió correctamente retorna la cantidad de bytes recibidos, sino retorna el código de error negativo. 
 */
int mqRecv(mqStruct *mqStruct_ptr, void *buffer, int size, long mtype);

#endif