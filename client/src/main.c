#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <pulse/gccmacro.h>
#include <signal.h>


#include "tcpConnection.h"
#include "udpConnection.h"

#define AUDIOBUFSIZE 1024

void sigKillHandler(int val);

int flag = 0;
/* Global declarations. */
tcpStruct tcp;
udpStruct udp;

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: client <host> <file.wav>\n");
        exit(1);
    }
    
    char buf[50] = " ";
    uint8_t udpBuf[AUDIOBUFSIZE];
    signal(SIGINT, sigKillHandler);
    tcpInitClient(&tcp, argv[1], 3457);
    
    tcpReceive(&tcp, buf, sizeof(buf), 0);

    int udpPort = atoi(buf);
    printf("Puerto udp: %d\n", udpPort);

    udpInitSenderFromRandomPort(&udp, argv[1], udpPort);
    printf("Puerto udp inicializado.\n");
    printf("Iniciando envío de archivo\n");
    // int fd = open(argv[1], O_RDONLY);
    // lseek(fd, 44, SEEK_SET);
    FILE *fp, *fp2;
    fp = fopen(argv[2], "rb");
    // fp2 = fopen("prueba.wav", "wb");
    fseek(fp, 48, SEEK_SET);
    int numBytesRead;
    int numBytesSend;
    long int cont = 0;
    while (1)
    {
        numBytesRead = fread(udpBuf, sizeof(uint8_t), AUDIOBUFSIZE, fp);
        if(numBytesRead == 0)
        {
            printf("EOF reached. \n");
            break;
        }
        numBytesSend = udpSend(&udp, udpBuf, numBytesRead);
        usleep(10000);
        cont += numBytesSend/1024;
    }

    printf("Kb enviados = %ld\n", cont);
    
    sleep(1);
    fclose(fp);
    udpClose(&udp);
    tcpSend(&tcp, "FIN", 4);
    tcpClose(&tcp);    
    return 0;
}

void sigKillHandler(int val)
{
    printf("Cerrando y saliendo\n");
    tcpSend(&tcp, "FIN", 4);
    tcpClose(&tcp);
    udpClose(&udp);
    exit(1);
}
