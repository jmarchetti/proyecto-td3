/*
 * ------------- UDP Connection Library --------------
 * 
 * Library for udp connections.
 */

#ifndef __UDPCONNECTION_H__
#define __UDPCONNECTION_H__
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>


typedef struct 
{
    int udpSockFd;
    struct sockaddr_in udpMyAddr;     /* contendra la direccion IP y el numero de puerto local */
    struct sockaddr_in udpTheirAddr;  /* Contendra la direccion IP y numero de puerto del cliente */
    int addr_len;

} udpStruct;


/* Function udpInit for sender side. 
 * Parameters:
 * - udpStruct_ptr: Tipo puntero tcpStruct. Puntero a la estructura a inicializar.
 * - hostname: Tipo pnuntero char. Hostname o ip al cual se debe enviar.
 * - port: Tipo entero. Puerto al cual se enviarán los datos.
 * Returns:
 *  - Si inicializó correctamente retorna 0, sino retorna el código de error negativo. 
 */

int udpInitSenderFromRandomPort(udpStruct *udpStruct_ptr, char *dest_hostname, int dest_port);

/* Function udpInit for receiver side. 
 * Parameters:
 * - udpStruct_ptr: Tipo puntero tcpStruct. Puntero a la estructura a inicializar.
 * - port: Tipo entero. Puerto en el cual se escucharán los datos.
 * Returns:
 *  - Si inicializó correctamente retorna 0, sino retorna el código de error negativo. 
 */

int udpInitReceiver(udpStruct *udpStruct_ptr, int local_port);


/* Function udpSend (bloqueante)
 * Parameters:
 *  - updStruct_ptr: Tipo puntero udpStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Tipo puntero void. Puntero a los datos a enviar.
 *  - size: Tipo entero. Tamaño en bytes de los datos a enviar.
 * Returns:
 *  - Si inicializó correctamente retorna la cantidad de bytes enviados, sino retorna el código de error negativo.
 */

int udpSend(udpStruct *udpStruct_ptr, const void *buffer, int size);

/* Function udpReceive (bloqueante)
 * Parameters:
 *  - udpStruct_ptr: Tipo puntero udpStruct. Puntero a la estructura ya inicializada.
 *  - buffer: Tipo puntero void. Puntero donde se guardarán los datos.
 *  - size: Tipo entero. Tamaño en bytes máximo de lectura.
 *  - timeout: Tipo entero. Timeout opcional. Si es 0, será completamente bloqueante.
 * Returns:
 *  - Si inicializó correctamente retorna la cantidad de bytes recibidos, sino retorna el código de error negativo.
 */

int udpReceive(udpStruct *udpStruct_ptr, void *buffer, int size, int timeout);

/* Function udpClose
 * Parameters:
 *  - udpStruct_ptr: Tipo puntero udpStruct. Puntero a la estructura ya inicializada.
 */

void udpClose(udpStruct *udpStruct_ptr);

#endif