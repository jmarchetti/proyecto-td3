#ifndef __PROCESSES_H__
#define __PROCESSES_H__
#include "tcpConnection.h"
#include "udpConnection.h"
#include "fifo.h"
#include "messageQueue.h"
#include <pulse/simple.h>
#include <pulse/error.h>
#include <pulse/gccmacro.h>

#define SIZE_OF_PACK 1024

typedef struct{

    tcpStruct childTCP;
    udpStruct childUDP;
    fifoStruct childCommandFifo;
    mqStruct childAudioMq;
    pid_t pid;
    unsigned char isSelected;

} childStruct;

typedef struct{
    pid_t pid;
    char extHost[20];
    int udpPort; //no se si lo voy a llegar a obtener.
    unsigned char isSelected;
    char fifoName[10];
}childInfo;

typedef struct{

    pa_simple *s;
    pa_sample_spec ss;
    int error;
} audioPlayer;

extern int childFlag;

/*
 * Function childNew.
 * Parameters:
 *  - childStruct_ptr: Tipo puntero a childStruct. Puntero a la estructura que contendrá toda la info.
 *  - childTCPStruct_ptr: Puntero a tcpStruct. Puntero a la estructura con los datos de la nueva conexión TCP.
 *  - udpInitialPort: Tipo entero.
 *  - audioMq_ptr: Puntero a estructura mqStruct. Puntero a estructura mq para envío de audio.s
 */
void childNew(childStruct *childStruct_ptr, tcpStruct *childTCPStruct_ptr, int udpInitialPort, mqStruct *audioMq_ptr, void *sigCHldHandler, void *sigUSR1Handler, void *sigIntHandler, int nchild);

/*
 * Function playerProcess.
 * Parameters:
 *  - audioMq_ptr: Tipo puntero a mqStruct. Puntero a una estructura Mq ya inicializada.
 *  - childStruct: Puntero a array de childs.
 *  - nchilds: Puntero a la cantidad de childs.
 */
void playerProcess(mqStruct *audioMq_ptr, audioPlayer *audioPlayer);

/* 
 * Function stdManager
 * Parameters:
 *  - childsArray: Tipo puntero childStruct. Puntero al array de estructuras de los hijos.
 *  - nchilds: puntero a int. Puntero a la variable nchilds.
 *  - logMqStruct: puntero a mqStruct. Puntero a un mqStruct ya inicializado para manejo de logs.
 */

// void stdManager(childStruct *childsArray, int *nchilds, mqStruct *logMqStruct);

/*
 * Function Mixer (asm).
 * Parameters:
 *  - dest: Puntero uint8_t. Puntero a donde se sumarán paquetes de 16 bytes.
 *  - src: Puntero uint8_t. Puntero desde donde se sumarán paquetes de 16 bytes.
 */
extern void Echo(uint8_t *src, uint8_t *dest);
#endif