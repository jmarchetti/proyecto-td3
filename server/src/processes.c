#include "processes.h"

/* Functions! */

static void initRep(audioPlayer *audioPlayer)
{
    audioPlayer->ss.format = PA_SAMPLE_S16LE;
    audioPlayer->ss.rate = 22050;
    audioPlayer->ss.channels = 2;
    audioPlayer->s = NULL;
    if (!(audioPlayer->s = pa_simple_new(NULL, "server", PA_STREAM_PLAYBACK, NULL, "playback", &(audioPlayer->ss), NULL, NULL, &(audioPlayer->error)))) {
        perror("Audio intialization error");
        if (audioPlayer->s)
        {
            pa_simple_free(audioPlayer->s);
        }
        
        exit(1);
    }
    printf("[INFO] Audio Player inicializado!\n");
}

static void rep(audioPlayer *audioPlayer, uint8_t *buffer, int size){
    if (pa_simple_write(audioPlayer->s, buffer, size, &(audioPlayer->error)) < 0) {
        perror("Audio player error");
        if (audioPlayer->s)
        {
            pa_simple_free(audioPlayer->s);
        }
        exit(1);
    }
}

void childNew(childStruct *childStruct_ptr, tcpStruct *childTCPStruct_ptr, int udpInitialPort, mqStruct *audioMq_ptr, void *sigCHldHandler, void *sigUSR1Handler, void *sigIntHandler, int nchild)
{
    int result;
    int udpPort = udpInitialPort;
    char buf[50] = " ";
    childStruct_ptr->pid = getpid();
    pid_t grandChildPid;
    // Copio la estructura TCP del hijo (ya está lista para usar).
    memcpy(&(childStruct_ptr->childTCP), childTCPStruct_ptr, sizeof(tcpStruct));
    // Inicalizo fifo para comandos
      
    char fifoName[20];
    sprintf(fifoName, "../ch%d", childStruct_ptr->pid);
    if ((result = fifoCreate(&(childStruct_ptr->childCommandFifo), fifoName)) < 0)
    {
        printf("errorFIFO:%s\n", strerror(-result));
    }
    printf("Soy el hijo con pid: %d\nValor child flag: %d\n", childStruct_ptr->pid, childFlag);
    
    do
    {
        result = udpInitReceiver(&childStruct_ptr->childUDP, udpPort);
        if (result == -EADDRINUSE && udpPort < udpInitialPort + 10)
        {
            udpPort++;
        }
        else
            if(result < 0)
            {
                printf("errorUDP:%s\n", strerror(-result));
                sprintf(buf, "FIN");
            }
        
    } while (result < 0);

    printf("%d-Puerto: %d\n", childStruct_ptr->pid, udpPort);
    sprintf(buf, "%d", udpPort);
    tcpSend(&childStruct_ptr->childTCP, buf,strlen(buf)+1);
    if(nchild==0)
    {
        childFlag = 1;
        childStruct_ptr->isSelected = 1;
    }
    if((grandChildPid = fork()) == 0)
    {
        uint8_t udpBuf[SIZE_OF_PACK];
        int num;
        signal(SIGUSR1, sigUSR1Handler);
        while(1)
        {
            num = udpReceive(&childStruct_ptr->childUDP, udpBuf, SIZE_OF_PACK, 0);
            if (childFlag==1)
            {
                mqSend(audioMq_ptr, udpBuf, SIZE_OF_PACK, childStruct_ptr->pid);
            }
        }
    }
    signal(SIGCHLD, sigCHldHandler);
    signal(SIGINT, sigIntHandler);
    fd_set fdSet;
    int fifoFd = open(childStruct_ptr->childCommandFifo.name, O_RDONLY | O_NONBLOCK);
    FD_ZERO(&fdSet);
    FD_SET(fifoFd, &fdSet);
    FD_SET(childStruct_ptr->childTCP.tcpSockFd, &fdSet);
    int maxFd = fifoFd;
    if(childStruct_ptr->childTCP.tcpSockFd > maxFd)
        maxFd = childStruct_ptr->childTCP.tcpSockFd;
    while (strcmp(buf, "FIN"))
    {
        select(maxFd + 1, &fdSet, NULL, NULL, NULL);
        if(FD_ISSET(fifoFd, &fdSet))
        {
            // fifoRead(&(childStruct_ptr->childCommandFifo), buf, 50);
            read(fifoFd, buf, 50);
            close(fifoFd);
            printf("[DEBUG] %d-FIFO: %s\n", childStruct_ptr->pid, buf);
            
            if(!strcmp(buf, "ts"))
            {
                printf("[DEBUG] %d-Seleccionando proceso.\n", childStruct_ptr->pid);
                if (childStruct_ptr->isSelected == 0)
                    childStruct_ptr->isSelected=1;
                else
                    childStruct_ptr->isSelected=0;
                kill(grandChildPid, SIGUSR1);
            }
        }
        else if(FD_ISSET(childStruct_ptr->childTCP.tcpSockFd, &fdSet))
        {
            close(fifoFd);
            tcpReceive(&childStruct_ptr->childTCP, buf, 50, 0);
            printf("[INFO] %d-TCP: %s\n",childStruct_ptr->pid, buf);
        }
        fifoFd = open(childStruct_ptr->childCommandFifo.name, O_RDONLY | O_NONBLOCK);
        FD_ZERO(&fdSet);
        FD_SET(fifoFd, &fdSet);
        FD_SET(childStruct_ptr->childTCP.tcpSockFd, &fdSet);
    }    
    kill(grandChildPid, SIGINT);
    tcpClose(&childStruct_ptr->childTCP);
    udpClose(&childStruct_ptr->childUDP);
    exit(0);
}

void playerProcess(mqStruct *audioMq_ptr, audioPlayer *audioPlayer)
{
    int nReads = 0;
    int result;
    int i;
    uint8_t buf[SIZE_OF_PACK];
    initRep(audioPlayer);
    while(1){

        while (result != -ENOMSG)
        {
            result = mqNonBlockingRecv(audioMq_ptr, buf, sizeof(buf), 0);
            if (result > 0 )
            {
                // nReads++;
                // if(childFlag)
                // {
                //     for ( i = 0; i < result / 16; i++)
                //     {
                //         Echo(buf + (i * 16), buf + (i * 16) + 512);
                //     }
                //     rep(audioPlayer, buf, SIZE_OF_PACK);
                // }
                // else
                // {
                rep(audioPlayer, buf, SIZE_OF_PACK);    
                // }
                

            }
            else
            {
                if(result < 0 && result != -ENOMSG)
                {
                    printf("errorRcvMQ:%s\n", strerror(-result));
                }
                sleep(1);
            }
        }
        result=0;
    }
    
}
