GLOBAL Echo

Echo:

    movdqu xmm0, [rdi]  ; guardo 16 bytes en xmm0

    pxor xmm1, xmm1
    pxor xmm2, xmm2
    punpcklbw xmm0, xmm1 ; desempaqueto 8 bytes bajos de xmm0 a words en xmm1 (lo hago a words para pode dividir por 2)
    punpckhbw xmm0, xmm2 ; desempaqueto 8 bytes altis de xmm0 a words en xmm2 (lo hago a words para poder dividir por 2)

    psrlw xmm1, 1   ; desplazo a derecha para dividir por 2 parte baja
    psrlw xmm2, 1   ; desplazo a derecha para dividir por 2 parte alta

    packuswb xmm1, xmm2 ;empaqueto a bytes saturando

    paddusb xmm1, [rsi] ; sumo al valor viejo dividido por 2 el valor nuevo 
    movdqu [rsi], xmm1 ; escribo el valor

    ret