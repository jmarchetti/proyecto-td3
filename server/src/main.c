#include <stdio.h>
#include "processes.h"
#include <signal.h>

#define MAX_CHLD 5
#define TCP_PORT 3457
#define UDP_INITIAL_PORT 10000

/* Global declarations. */
tcpStruct masterTcp;
childInfo childs[MAX_CHLD];
mqStruct audioMq;
fifoStruct fifoChildINFO;
fifoStruct fifoChildCommand;
int nchilds = 0;
int selectedId = 0;
int childFlag = 0;
/* Function prototypes */
void sigChldHandler(int val);
void chldSigChldHandler(int val);
void chldSigUSR1Handler(int val);
void sigIntHandler(int val);
void deleteChild(childInfo *chInfo, pid_t chPid, int localNchilds);
int getChildId(childInfo *chInfo, pid_t chPid, int localNchilds);
int splitBySpace(char *text, char *textArray[], int arraySize);

struct childInfo
{
    pid_t pid;
};

int main()
{
    int result;
    int serverPID, mixerPID;
    tcpStruct childTCPStruct;
    childStruct newChild;
    // Inicializo TCP Master
    if ((result = tcpInitServer(&masterTcp, TCP_PORT)) < 0)
    {
        printf("errorInitMasterTCP: %s\n", strerror(-result));
    }
    if((result = mqInit(&audioMq, "../.randomfile", 'B', 2048))<0)
    {
        printf("errorInitMQ: %s\n", strerror(-result));
    }
    if((mixerPID = fork()) == 0)
    {
        audioPlayer audioPlayer;
        playerProcess(&audioMq, &audioPlayer);
    }
    fifoCreate(&fifoChildINFO, "../childINFO");

    if((serverPID = fork()) == 0)
    {   
        char testBuffer[50];
        signal(SIGCHLD, sigChldHandler);
        // signal(SIGINT, sigIntHandler);
        while (nchilds < MAX_CHLD)
        {
            if ((result = tcpAccept(&masterTcp, &childTCPStruct, 0)) < 0)
            {
                printf("errorAcceptNewTCP: %s\n", strerror(-result));
            }
            result = fork();
            switch (result)
            {
                case -1:
                    perror("fork");
                break;
            
                case 0:
                    childNew(&newChild, &childTCPStruct, UDP_INITIAL_PORT, &audioMq, chldSigChldHandler, chldSigUSR1Handler, sigIntHandler , nchilds);
                break;
                default:
                    printf("[INFO] \n\nHijo creado con pid %d.\n", result);
                    printf("[DEBUG] SERVER-FifoSend: n%d-%s\n\n", result, inet_ntoa(childTCPStruct.tcpTheirAddr.sin_addr));
                    sprintf(testBuffer, "n%d-%s", result, inet_ntoa(childTCPStruct.tcpTheirAddr.sin_addr));
                    fifoWrite(&fifoChildINFO, testBuffer, strlen(testBuffer) + 1);
                    nchilds++;
                    break;
            }
        }
    }
    signal(SIGINT, sigIntHandler);
    fd_set fdSet;
    char command[20];
    char fifoBuffer[50];
    FD_ZERO(&fdSet);
    int nchilds2 = 0;
    int childId;
    int fifoFd = open(fifoChildINFO.name, O_RDONLY | O_NONBLOCK);
    FD_SET(0, &fdSet);
    FD_SET(fifoFd, &fdSet);
    int maxfd = 0;
    int splitedSize;
    char *splitedCommandBuffer[5];
    if(fifoFd > maxfd)
        maxfd=fifoFd;
    
    while (select(maxfd+1, &fdSet, NULL, NULL, NULL))
    {
        // close(fifoFd);
        if(FD_ISSET(fifoFd, &fdSet))
        {
            // fifoRead(&fifoChildINFO, fifoBuffer, 50);
            read(fifoFd, fifoBuffer, sizeof(fifoBuffer));
            close(fifoFd);
            printf("\n[INFO] MASTER-FIFORcv: %s\n", fifoBuffer);
            if(fifoBuffer[0] == 'n')
            {
                //New child
                char *aux = strchr(fifoBuffer, '-');
                strncpy(command, fifoBuffer+1, aux - fifoBuffer + 1);
                // command[5] = '\0';
                childs[nchilds2].pid = atoi(command);
                strcpy(childs[nchilds2].extHost, aux+1);
                sprintf(childs[nchilds2].fifoName, "ch%d", childs[nchilds2].pid);
                printf("\n\n ID\t PID \t   IP  \n");
                if(nchilds2 == 0)
                {
                    childs[nchilds2].isSelected = 1;
                    printf("*%d\t%d\t%s\n", nchilds2, childs[nchilds2].pid, childs[nchilds2].extHost);
                }
                else
                {
                    printf(" %d\t%d\t%s\n", nchilds2, childs[nchilds2].pid, childs[nchilds2].extHost);
                }
                nchilds2++;
            }
            else if (fifoBuffer[0] == 'd')
            {
                //Delete child.
                // Me tengo que fijar si está seleccionado.
                printf("[INFO] MASTER-Borrando hijo %s\n", fifoBuffer+1);
                strcpy(command, fifoBuffer+1);
                int childId = getChildId(childs, atoi(command), nchilds2);
                if (childs[childId].isSelected){
                    selectedId = -1;
                }
                
                deleteChild(childs, atoi(command), nchilds2);
                nchilds2--;
            }
            
            
        }
        else
            if(FD_ISSET(STDIN_FILENO, &fdSet))
            {
                close(fifoFd);
                fgets(command, 20, stdin);
                if( strlen(command) > 0 && command[strlen(command) - 1 ] == '\n' )
                    command[strlen(command) - 1 ] = '\0';
                fflush(stdin);
                fflush(stdin);
                //parto el mensaje en los espacios
                splitedSize = splitBySpace(command, splitedCommandBuffer, 5);
                if(splitedSize > 0)
                {
                    if(!strcmp(splitedCommandBuffer[0], "list"))
                    {
                        printf("------ Procesos abiertos -------\n");
                        if(nchilds2 == 0)
                        {
                            printf("No existen procesos abiertos.\n");
                        }
                        else
                        {
                            printf(" ID\t PID \t   IP  \n");
                            for (int i = 0; i < nchilds2; i++)
                            {
                                if(childs[i].isSelected)
                                    printf("*%d\t%d\t%s\n", i, childs[i].pid, childs[i].extHost);
                                else
                                    printf(" %d\t%d\t%s\n", i, childs[i].pid, childs[i].extHost);
                            }
                        }
                        
                    }
                    else if (!strcmp(splitedCommandBuffer[0], "p"))
                    {
                        printf("\n---- Variables ---- \n");
                        printf("Nchilds: %d\n", nchilds2);
                    }
                    else if (!strcmp(splitedCommandBuffer[0], "select"))
                    {

                        // printf("\n[INFO] MASTER-Seleccionando Proceso %d.\n", atoi(splitedCommandBuffer[1]));
                        // Deselecciono al actual.
                        if ((childId = getChildId(childs, atoi(splitedCommandBuffer[1]), nchilds2)) != -1){
                            if (selectedId != -1){
                                printf("[INFO] MASTER-Selected id: %d\n", selectedId);
                                strcpy(fifoBuffer, "ts");
                                sprintf(fifoChildCommand.name, "../ch%d", childs[selectedId].pid);
                                if ((result = fifoWrite(&fifoChildCommand, fifoBuffer, strlen(fifoBuffer) + 1)) < 0)
                                {
                                    printf("fifoWriteError: %s\n", strerror(-result));
                                }
                                childs[selectedId].isSelected=0;
                            }
                            
                            if(selectedId == -1 || atoi(splitedCommandBuffer[1]) != childs[selectedId].pid)
                            {
                                // Selecciono al indicado
                                selectedId = getChildId(childs, atoi(splitedCommandBuffer[1]), nchilds2);
                                childs[selectedId].isSelected=1;
                                sprintf(fifoChildCommand.name, "../ch%d", atoi(splitedCommandBuffer[1]));                        
                                strcpy(fifoBuffer, "ts");
                                if ((result = fifoWrite(&fifoChildCommand, fifoBuffer, strlen(fifoBuffer) + 1)) < 0)
                                {
                                    printf("fifoWriteError: %s\n", strerror(-result));
                                }
                            }
                            else
                            {
                                selectedId = -1;
                            }
                        }
                        else
                        {
                            printf("\nChild ID not found\n");
                        }
                    }
                    else if (!strcmp(splitedCommandBuffer[0], "purge"))
                    {
                        for (int i = 0; i < nchilds2; i++)
                        {
                            printf("[INFO] Purging child %d.\n", childs[i].pid);
                            deleteChild(childs, childs[i].pid, nchilds2);
                            nchilds2--;
                        }
                        nchilds2=0;
                    }
                    else if (!strcmp(splitedCommandBuffer[0], "exit"))
                    {
                        kill(0, SIGINT);
                        exit(0);
                    }
                    
                    // printf("STDIN: %s\n", command);

                }
            }
        FD_ZERO(&fdSet);
        fifoFd = open(fifoChildINFO.name, O_RDONLY | O_NONBLOCK);
        FD_SET(0, &fdSet);
        FD_SET(fifoFd, &fdSet);
    }    
    return 0;
}

void sigChldHandler(int val)
{
    int pid  = wait(0);
    char aux[10];
    printf("[INFO] SERVER: Hijo %d destruido. Enviando d%d\n", pid, pid);
    sprintf(aux, "d%d", pid);
    fifoWrite(&fifoChildINFO, aux, strlen(aux)+1);
    nchilds--;
    printf("Quedan %d childs.\n", nchilds);
    usleep(10000);
}

void chldSigChldHandler(int val)
{
    usleep(1000);
}
void sigIntHandler(int val){
    // printf("\n\nUsar el comando 'exit' para cerrar el servidor.\n");
    kill(0, SIGINT);
    sleep(2);
    exit(0);
}
void chldSigUSR1Handler(int val){
    if (childFlag == 0)
    {
        childFlag = 1;
    }
    else
    {
        childFlag = 0;
    }
}

void deleteChild(childInfo *chInfo, pid_t chPid, int localNchilds)
{
    int ID, i;
    for (i = 0; i < localNchilds; i++)
    {
        if (chInfo[i].pid == chPid)
        {
            ID = i;
            break;
        }
    }
    childInfo aux[MAX_CHLD];
    memcpy(aux, chInfo, MAX_CHLD * sizeof(childInfo));
    for (i = localNchilds; i > ID; i--)
    {
        chInfo[i-1] = aux[i];
    }
    for(i = ID -1; i >= 0; i--)
    {
        chInfo[i] = aux[i];
    }
    
    
}

int getChildId(childInfo *chInfo, pid_t chPid, int localNchilds)
{
    int result = -1;
    for ( int i = 0; i < localNchilds; i++)
    {
        if(chInfo[i].pid == chPid)
        {
            result = i;
            break;
        }
    }
    return result;
}

int splitBySpace(char *text, char *textArray[], int arraySize)
{
    char delim[] = " ";
    int i = 0;
    char *ptr = strtok(text, delim);

    while(ptr != NULL && i < arraySize)
    {
        textArray[i] = ptr;
        i++;
        ptr = strtok(NULL, delim);
    }
    return i;
}