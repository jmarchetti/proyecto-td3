# include "messageQueue.h"

/* Functions */

int mqInit(mqStruct *mqStruct_ptr, const char *key, char keyChar, int messageSize){
    // mqStruct_ptr->mQ.message = malloc(messageSize);
    if ((mqStruct_ptr->mqKey = ftok(key, keyChar)) == -1)
    {
        return -(errno);
    }
    if ((mqStruct_ptr->mqId = msgget(mqStruct_ptr->mqKey, 0644 | IPC_CREAT)) == -1)
    {
        return -(errno);
    }
    return 0;
}

int mqSend(mqStruct *mqStruct_ptr, void *buffer, int size, long mtype){

    memcpy(mqStruct_ptr->mQ.message, buffer, size);
    mqStruct_ptr->mQ.mtype = mtype;
    
    if (msgsnd(mqStruct_ptr->mqId, &(mqStruct_ptr->mQ), size, 0) == -1)
    {
        return -(errno);
    }
    else
    {
        return 0;
    }
    
}

int mqBlockingRecv(mqStruct *mqStruct_ptr, void *buffer, int size, long mtype){
    int numBytes;
    if((numBytes = msgrcv(mqStruct_ptr->mqId, &(mqStruct_ptr->mQ), sizeof(mqStruct_ptr->mQ), mtype, 0)) == -1)
    {
        return -(errno);
    }
    memcpy(buffer, mqStruct_ptr->mQ.message, numBytes);

    return numBytes;
    
}

int mqNonBlockingRecv(mqStruct *mqStruct_ptr, void *buffer, int size, long mtype){
    int numBytes;
    if((numBytes = msgrcv(mqStruct_ptr->mqId, &(mqStruct_ptr->mQ), sizeof(mqStruct_ptr->mQ), mtype, IPC_NOWAIT)) == -1)
    {
        return -(errno);
    }
    
    memcpy(buffer, mqStruct_ptr->mQ.message, numBytes);

    return numBytes;    
}
