#include "udpConnection.h"


/*
 * Functions!
 */

int udpInitSenderFromRandomPort(udpStruct *udpStruct_ptr, char *dest_hostname, int dest_port){
    struct hostent *he;
    if ((he = gethostbyname(dest_hostname)) == NULL)
    {
        return -(errno);
    }
    
    if ((udpStruct_ptr->udpSockFd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        return -(errno);
    }
    udpStruct_ptr->udpTheirAddr.sin_family = AF_INET;
    udpStruct_ptr->udpTheirAddr.sin_port = htons(dest_port);
    udpStruct_ptr->udpTheirAddr.sin_addr = *((struct in_addr *)he->h_addr);
    bzero(&(udpStruct_ptr->udpTheirAddr.sin_zero), 8);
    return 0;
}

int udpSend(udpStruct *udpStruct_ptr, const void *buffer, int size){
    int numbytes;
    if ((numbytes = sendto(udpStruct_ptr->udpSockFd, buffer, size, 0, (struct sockaddr *)&(udpStruct_ptr->udpTheirAddr), sizeof(struct sockaddr))) == -1 )
    {
        return -(errno);
    }
    else
    {
        return numbytes;
    }
    
    
}

int udpInitReceiver(udpStruct *udpStruct_ptr, int local_port){
    
    if ((udpStruct_ptr->udpSockFd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        return -(errno);
    }

    udpStruct_ptr->udpMyAddr.sin_family = AF_INET;
    udpStruct_ptr->udpMyAddr.sin_port = htons(local_port);
    udpStruct_ptr->udpMyAddr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(udpStruct_ptr->udpMyAddr.sin_zero),8);

    if (bind(udpStruct_ptr->udpSockFd, (struct sockaddr *)&(udpStruct_ptr->udpMyAddr), sizeof(struct sockaddr)) == -1)
    {
        return -(errno);
    }

    udpStruct_ptr->addr_len = sizeof(struct sockaddr);

    return 0;
}

int udpReceive(udpStruct *udpStruct_ptr, void *buffer, int size, int timeout){
    struct timeval timeoutStruct;
    fd_set receiveFdSet;
    int select_result;
    int numBytes;
    if (timeout > 0)
    {
        timeoutStruct.tv_sec = timeout;
        FD_ZERO(&receiveFdSet);
        FD_SET(udpStruct_ptr->udpSockFd, &receiveFdSet);
        select_result = select(udpStruct_ptr->udpSockFd + 1, &receiveFdSet, NULL, NULL, &timeoutStruct);
        if ( select_result == -1 ){
            return -(errno);
        }
        else 
            if (FD_ISSET(udpStruct_ptr->udpSockFd, &receiveFdSet))
            {
                if ((numBytes = recvfrom(udpStruct_ptr->udpSockFd, buffer, size, 0, (struct sockaddr *)&(udpStruct_ptr->udpTheirAddr), &(udpStruct_ptr->addr_len))) == -1){
                    return -(errno);
                }
                else
                {
                    return numBytes;
                }
            }
            else
            {
                return 0;
            }        
    }
    else
    {
        if ((numBytes = recvfrom(udpStruct_ptr->udpSockFd, buffer, size, 0, (struct sockaddr *)&(udpStruct_ptr->udpTheirAddr), &(udpStruct_ptr->addr_len))) == -1){
            return -(errno);
        }
        else
        {
            return numBytes;
        }
    }
}

void udpClose(udpStruct *udpStruct_ptr){
    close(udpStruct_ptr->udpSockFd);
}