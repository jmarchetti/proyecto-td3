# include "fifo.h"

/* Functions */

int fifoCreate(fifoStruct *fifoStruct_ptr, const char *name){
    strcpy(fifoStruct_ptr->name, name);
    if ((mknod(fifoStruct_ptr->name, S_IFIFO | 0777, 0)) == -1)
    {
        return -(errno);
    }
    else
    {
        return 0;
    }
}


int fifoWrite(fifoStruct *fifoStruct_ptr, void *buffer, int size){
    int numBytes;
    int fifoFd;

    if ((fifoFd = open(fifoStruct_ptr->name, O_WRONLY)) == -1)
    {
        return -(errno);
    }

    if ((numBytes = write(fifoFd, buffer, size)) == -1)
    {
        return -(errno);
    }
    close(fifoFd);
    return numBytes;
}

int fifoRead(fifoStruct *fifoStruct_ptr, void *buffer, int size){
    int numBytes;
    int fifoFd;

    if ((fifoFd = open(fifoStruct_ptr->name, O_RDONLY | O_NONBLOCK)) == -1)
    {
        return -(errno);
    }

    if ((numBytes = read(fifoFd, buffer, size)) == -1)
    {
        return -(errno);
    }
    
    close(fifoFd);
    return numBytes;
}