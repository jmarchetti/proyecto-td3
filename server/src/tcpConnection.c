#include "tcpConnection.h"


/*
 * Functions!
 */

int tcpInitServer(tcpStruct *tcpStruct_ptr, int port){
    
    // Creo socket TCP
    if ((tcpStruct_ptr->tcpSockFd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        return -(errno);
    }

    tcpStruct_ptr->tcpMyAddr.sin_family = AF_INET;
    tcpStruct_ptr->tcpMyAddr.sin_addr.s_addr = INADDR_ANY;   
    tcpStruct_ptr->tcpMyAddr.sin_port = htons(port);
    bzero(&(tcpStruct_ptr->tcpMyAddr.sin_zero), 8);  /* rellena con ceros el resto de la estructura */

    if ( bind(tcpStruct_ptr->tcpSockFd, (struct sockaddr *)&tcpStruct_ptr->tcpMyAddr, sizeof(struct sockaddr)) == -1)
    {
        return -(errno);
    }

    if (listen(tcpStruct_ptr->tcpSockFd, BACKLOG) == -1)
    {
        return -(errno);
    }
    tcpStruct_ptr->sinSize = sizeof(struct sockaddr_in);

    return 0;
}

int tcpInitClient(tcpStruct *tcpStruct_ptr, char *server, int port){
    // Creo socket TCP
    if ((tcpStruct_ptr->tcpSockFd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        return -(errno);
    }
    struct hostent *he;
    he = gethostbyname(server);
    tcpStruct_ptr->tcpTheirAddr.sin_family = AF_INET;
    tcpStruct_ptr->tcpTheirAddr.sin_port = htons(port);
    tcpStruct_ptr->tcpTheirAddr.sin_addr = *((struct in_addr*)he->h_addr);
    memset (&(tcpStruct_ptr->tcpTheirAddr.sin_zero),'\0',8);

    connect(tcpStruct_ptr->tcpSockFd, (struct sockaddr *)&tcpStruct_ptr->tcpTheirAddr, sizeof(struct sockaddr));
    
}


int tcpAccept(tcpStruct *tcpStruct_ptr, tcpStruct *tcpStructNew_ptr, int timeout){

    struct timeval timeoutStruct;
    fd_set acceptFdSet;
    if (timeout > 0)
    {
        timeoutStruct.tv_sec = timeout;
        FD_ZERO(&acceptFdSet);
        FD_SET(tcpStruct_ptr->tcpSockFd, &acceptFdSet);
        if ( (select(tcpStruct_ptr->tcpSockFd + 1, &acceptFdSet, NULL, NULL, &timeoutStruct) == -1) ){
            if (errno == 4)
            {
                return 0;
            }
            else
            {
                return -(errno);
            }
        }
        else
        {
            if ((tcpStructNew_ptr->tcpSockFd = accept(tcpStruct_ptr->tcpSockFd, (struct sockaddr *)&tcpStructNew_ptr->tcpTheirAddr, &tcpStruct_ptr->sinSize)) == -1){
                return -(errno);
            }
            else
            {
                return 1;
            }
            
        }
        
    }
    else
    {
        if ((tcpStructNew_ptr->tcpSockFd = accept(tcpStruct_ptr->tcpSockFd, (struct sockaddr *)&tcpStructNew_ptr->tcpTheirAddr, &tcpStruct_ptr->sinSize)) == -1){
            return -(errno);
        }
        else
        {
            return 1;
        }
    }
    
}

int tcpSend(tcpStruct *tcpStruct_ptr, const void *buffer, int size){
    int numBytes;
    if ((numBytes = send(tcpStruct_ptr->tcpSockFd, buffer, size, 0))== -1)
    {
        return -(errno);
    }
    else
    {
        return numBytes;
    }
    
    
}

int tcpReceive(tcpStruct *tcpStruct_ptr, void *buffer, int size, int timeout){
    struct timeval timeoutStruct;
    fd_set receiveFdSet;
    int select_result;
    int numBytes;
    if (timeout > 0)
    {
        timeoutStruct.tv_sec = timeout;
        FD_ZERO(&receiveFdSet);
        FD_SET(tcpStruct_ptr->tcpSockFd, &receiveFdSet);
        select_result = select(tcpStruct_ptr->tcpSockFd + 1, &receiveFdSet, NULL, NULL, &timeoutStruct); 
        if ( select_result == -1 ){
                return -(errno);
        }
        else
            if(select_result)
            {
                if ((numBytes = recv(tcpStruct_ptr->tcpSockFd, buffer, size, 0)) == -1){
                    return -(errno);
                }
                else
                {
                    return numBytes;
                }
            }
            else
            {
                /* Timeout */
                return 0;
            }
            
        
    }
    else
    {
        if ((numBytes = recv(tcpStruct_ptr->tcpSockFd, buffer, size, 0)) == -1){
            return -(errno);
        }
        else
        {
            return numBytes;
        }
    }
}

void tcpClose(tcpStruct *tcpStruct_ptr){
    close(tcpStruct_ptr->tcpSockFd);
}